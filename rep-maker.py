#!/usr/bin/python

# got this from https://bitbucket.org/neil_rubens/rapidminer_maven-repo/wiki/Home
# now edited to change from install (for local maven repo) to deploy (for remote repo)

import sys
import os.path
import subprocess


if len(sys.argv) < 2:
    sys.stderr.write('Usage: full_path_to_jars_dir RapidMiner-Ver\n')
    sys.exit(1)

folder = sys.argv[1]
rm_ver = sys.argv[2]
groupId = 'xyz.monkeyballs'
current_dir = os.path.dirname(os.path.realpath(__file__))
repo_url_local = 'file://' + current_dir + '/repository/'

#subprocess.call(["ls", "-l"])

f = open('dependencies.xml', 'w')
f_commands = open('mvn_commands', 'w')

print 'processing: \n'
for filename in os.listdir (folder):
	if filename.endswith('.jar') == True:
		print filename + '\n'
        # create dependencies for pom.xml
        f.writelines('<dependency>\n')
        f.writelines('<groupId>' + groupId + '</groupId>\n')
        f.writelines('<artifactId>' + filename[:-4] + '</artifactId>\n')
        f.writelines('<version>' + rm_ver + '</version>\n')
        f.writelines('</dependency>\n\n')

        # create dependencies for pom.xml
        command = 'mvn deploy:deploy-file -DgroupId=' + groupId + ' -DartifactId='+ filename[:-4] + ' -Dversion=' + rm_ver + ' -Dfile=' + folder + '/' + filename + ' -Dpackaging=jar -DgeneratePom=true -Durl=' + repo_url_local + ' -DcreateChecksum=true'
        f_commands.write(command + '\n')


f.close()
f_commands.close()

print 'TODO: \n'
print f_commands.name + ' containts mvn commands that need to be executed; may need to do chmod + x'
print f.name + ' containts dependencies that need to be added to pom.xml'
print 'Dont forget to commit to git/maven repository'
