# README #

#-------#
Quick tip: If you want to update your local cached (maven) versions of the repo do:

1) cd to the directory of the project whose dependencies you want to update, so that the pom.xml file appears when you 'ls'.\n

2) run 'mvn clean install -U'\n

3) Refresh eclipse project\n

For gradle:
1) Run 'rm -r' on ~/.gradle and ~/.m2

2) from android-studio File > Synchronize (Ctrl+Alt+Y) on project to redownload dependencies
#-------#
This repo is based on the template given here: https://bitbucket.org/neil_rubens/rapidminer_maven-repo/wiki/Home

And here's the copied wiki page just in case.

This wiki explains how to host maven repository for third party jars on Bitbucket (this is also applicable to other git hosts e.g. GitHub (with minor modifications); but I do recommend Bitbucket). 

This repository includes  `rep-maker.py` script that partially automates adding a large number of jars to maven repository and provides dependency info for pom.xml (described in the latter section).

*Note: the proper way is to add the jars to the central maven repository; but this may not always be feasible especially for a large number of third party jars. Hence, the solution described here is a hack.*

# Problem #

Not all of the jars are in the maven central repository, or in any publicly accessible repository.

p.s. For our case study we use RapidMiner which is currently build with ant, and contains a large number of third party jars which were not in publicly accessible maven repo (are now in this unofficial maven repo).

# Solution #

Maven repository is just a collection of folders/files so could be easily hosted on git.  p.s. also see alternatives section towards the end.

## Install jars in a local maven repository ##

Make a directory e.g. `maven-repo` with the following subfolders:

* `jars` should contain the jar files that you want the script to add to maven's repository
* `repository` contains local maven repository; for now it is empty later on will be populated by `mvn`

Script `rep-maker.py` takes two arguments: `full_path_to_jars_dir lib_ver`

Executing `rep-maker.py` that will create two files:

* `mvn_commands` contains commands that should be executed to populate maven's `repository` folder 
* `dependencies.xml` that should be added to your `pom.xml`

Execute `mvn_commands`, it will add all of the `jars` to `repository` folder (don't forget to do `chmod +x mvn_commands`).

Create a project on Bitbucket; named e.g. <APP-NAME>_maven-repo; and add all of the files from `maven-repo` folder to it.

To point maven to it; add the following to your pom.xml (modifing appropriate values it to point to your project instead of mine):

```
#!xml
    <!-- Maven Repository on Git-BitBucket -->
    <repositories>
        <repository>
            <id>neil_rubens-repository</id>
            <url>https://bitbucket.org/neil_rubens/rapidminer_maven-repo/raw/master/repository/</url>
        </repository>
    </repositories>
```

Now you can add dependencies from `dependencies.xml` to your `pom.xml`

## Creating Encapsulating Artifact ##

The number of dependencies could be quite large; we can create an encapsulating artifact with all of the dependencies.  cd into `encapsulate` folder, then update the pom.xml with the dependencies; and repository location information.  Then execute:
`mvn package`, this is necessary since we need a jar to install (even though the jar is basically empty).  Then to install the jar: 

```
#!bash

mvn install:install-file -DpomFile=pom.xml -Dfile=./target/<YOUR-JAR>.jar -DlocalRepositoryPath=../repository  -DcreateChecksum=true
```



# Tips #

To start fresh; you can just delete everything from `repository` and re-run the `mvn-commands`

# Other #

For additional info see [my post](http://activeintelligence.org/blog/archive/hosting-maven-repository-for-third-party-jars-on-git-
bitbucketgithub/).